namespace :dev do
  desc "Configura o ambiente dev"
  task setup: :environment do
    puts "Resetando o banco de dados...."
    %x(rails db:drop db:create db:migrate)

    puts "Cadastrando os tipos contatos"

    kinds = %w(Amigo Comercial Conhecido)

    kinds.each do |kind|
      Kind.create!(
        description: kind
      )
    end

    puts "Tipos de contatos cadastrados com sucesso"

    ######################

    puts "Cadastrando contatos"

    100.times do |i|
      Contact.create!(
        description: Faker::Name.name,
        email: Faker::Internet.email,
        birthdate: Faker::Date.between(from: '2014-09-23', to: '2014-09-25'), 
        kind: Kind.all.sample
      )
    end
    
    puts "Contatos cadastrados com sucesso"
    
    ######################

    puts "Cadastrando os telefones..."

    Contact.all.each do |contact|
      Random.rand(5).times do |i|
        phone = Phone.create!(number:Faker::PhoneNumber.cell_phone)
        contact.phones << phone
        contact.save!
      end
    end

    puts "Telefones cadastrados com sucesso!"

    ######################

    puts "Cadastrando os enderecos..."

    Contact.all.each do |contact|
      Address.create(
        street: Faker::Address.street_address,
        city: Faker::Address.city,
        contact: contact
      )

    end

    puts "Enderecos cadastrados com sucesso!"
  end
end


